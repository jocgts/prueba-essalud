import { AuthConfig } from 'angular-oauth2-oidc';
import { environment } from '../../environments/environment';

export const authConfig: AuthConfig = {
  loginUrl: environment.loginUrl,
  redirectUri: window.location.origin + environment.redirect,
  clientId: environment.clientId,
  scope: 'read',
  showDebugInformation: true,
  requireHttps: false,
  //logoutUrl: 'http://localhost:8080/authoriza/logout',
  // silentRefreshRedirectUri: window.location.origin + '/silent-refresh.html',
  oidc: false
};
