import { Injectable, ɵConsole } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Compendio } from '../models/compendio.model';
import { Detalle } from '../models/detalle.model';

@Injectable({
  providedIn: 'root'
})
export class CodigoService {
  private urlEndPoint = environment.apiUrl;
  constructor(private http: HttpClient) {}

  getCompendios(page: number): Observable<any> {
    return this.http.get(this.urlEndPoint + '/compendio/page/' + page);
  }
  filtrarCompendios(term: string, page: number): Observable<any> {
    console.log(page + '***' + term);

    return this.http.get(
      this.urlEndPoint + '/compendio/filtrar-descri/' + term + '/' + page
    );
  }
  getCompendio(ide): Observable<Compendio> {
    console.log('##' + ide);
    return this.http
      .get<Compendio>(`${this.urlEndPoint}/compendio/${ide}`)
      .pipe(
        catchError(e => {
          if (e.status !== 401 && e.error.mensaje) {
            console.error(e.error.mensaje);
          }
          return throwError(e);
        })
      );
  }
  saveCompendio(compendio: Compendio): Observable<Compendio> {
    return this.http
      .post(`${this.urlEndPoint}/compendio`, compendio)
      .pipe(map((response: any) => response as Compendio));
  }
  getDetallesPorCompendio(ide: number): Observable<Detalle[]> {
    return this.http.get<Detalle[]>(
      `${this.urlEndPoint}/compendio/detalle/filtrar-tabla/${ide}`
    );
  }
  getDetalle(ide): Observable<Detalle> {
    return this.http
      .get<Detalle>(`${this.urlEndPoint}/compendio/detalle/filtrar-id/${ide}`)
      .pipe(
        catchError(e => {
          if (e.status !== 401 && e.error.mensaje) {
            console.error(e.error.mensaje);
          }
          return throwError(e);
        })
      );
  }
  saveDetalle(detalle: Detalle): Observable<Detalle> {
    return this.http
      .post(`${this.urlEndPoint}/compendio/detalle`, detalle)
      .pipe(
        map((response: any) => response.detalle as Detalle),
        catchError(e => throwError(e))
      );
  }
  deleteDetalle(ide: number): Observable<Detalle> {
    return this.http
      .delete<Detalle>(`${this.urlEndPoint}/compendio/detalle/${ide}`)
      .pipe(catchError(e => throwError(e)));
  }
}
