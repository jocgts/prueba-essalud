import { Injectable } from '@angular/core';

import { HttpClientModule } from '@angular/common/http';

import { JwksValidationHandler, OAuthService } from 'angular-oauth2-oidc';
import { authConfig } from '../autenticacion/auth.config';
import { Usuario } from '../models/usuario.model';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private _usuario: Usuario;
  private _token: string;
  public get token(): string {
    if (this._token != null) {
      return this._token;
    } else if (
      this._token == null &&
      sessionStorage.getItem('access_token') != null
    ) {
      this._token = sessionStorage.getItem('access_token');
      this.guardarUsuario(this._token);
      return this._token;
    }
    return null;
  }
  public get usuario(): Usuario {
    if (this._usuario != null) {
      return this._usuario;
    } else if (
      this._usuario == null &&
      sessionStorage.getItem('usuario') != null
    ) {
      this._usuario = JSON.parse(sessionStorage.getItem('usuario')) as Usuario;
      return this._usuario;
    }
    return new Usuario();
  }
  constructor(
    private http: HttpClientModule,
    private oauthService: OAuthService
  ) {
    console.log('AUTH CONSTRUCTOR');
    this.configureOauthApi();
    this.oauthService.tokenValidationHandler = new JwksValidationHandler();
    this.oauthService.tryLogin({});
  }

  private configureOauthApi() {
    this.oauthService.configure(authConfig);
    this.oauthService.setupAutomaticSilentRefresh();
  }

  getAccessToken(): string {
    return this.oauthService.getAccessToken();
  }

  loadUserDetails() {
    this.oauthService.loadUserProfile();
  }

  get access_token_expiration() {
    return this.oauthService.getAccessTokenExpiration();
  }

  public login() {
    this.oauthService.initImplicitFlow();
  }
  public logout() {
    this.oauthService.logOut(false);
    // tslint:disable-next-line:no-unused-expression
    //this.oauthService.logoutUrl;
    //location.reload();
    window.location.href = 'http://localhost:8080/authoriza/logout';
  }
  isLoggedIn() {
    if (this.oauthService.getAccessToken() === null) {
      return false;
    }
    return true;
  }
  private getClaimEntry(entry) {
    var claims = this.oauthService.getIdentityClaims();

    if (!claims) {
      this.oauthService.loadUserProfile();
    } else {
      return claims[entry];
    }
  }
  guardarUsuario(accessToken: string): Usuario {
    let payload = this.obtenerDatosToken(accessToken);
    this._usuario = new Usuario();
    this._usuario.email = payload.correoUsuario;
    this._usuario.roles = payload.authorities;
    this._usuario.ruc = payload.rucEmpleadorUsuario;
    /*  this._usuario.codUsuario = payload.numeroDocumentoUsuario;*/
    this._usuario.idUsuario = payload.idnumerico;
    /* this._usuario.codDependenciaOspe = payload.codDependenciaOspe;*/
    sessionStorage.setItem('usuario', JSON.stringify(this._usuario));
    return this._usuario;
  }
  obtenerDatosToken(accessToken: string): any {
    if (accessToken != null) {
      return JSON.parse(atob(accessToken.split('.')[1]));
    }
    return null;
  }
}
