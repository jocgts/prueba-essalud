import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListaCompendioComponent } from './codigos/compendio/lista-compendio/lista-compendio.component';
import { NuevoCompendioComponent } from './codigos/compendio/nuevo-compendio/nuevo-compendio.component';
import { EditarCompendioComponent } from './codigos/compendio/editar-compendio/editar-compendio.component';
import { ListaDetalleComponent } from './codigos/detalle/lista-detalle/lista-detalle.component';
import { NuevoDetalleComponent } from './codigos/detalle/nuevo-detalle/nuevo-detalle.component';
import { EditarDetalleComponent } from './codigos/detalle/editar-detalle/editar-detalle.component';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { AuthGuard } from './autenticacion/auth.guard';
const routes: Routes = [
  { path: '', redirectTo: '/consulta-ppt', pathMatch: 'full' },
  {
    path: 'consulta-ppt',
    component: ListaCompendioComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'listaCompendio/page/:page',
    component: ListaCompendioComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'nuevoCompendio',
    component: NuevoCompendioComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'editarCompendio/:id',
    component: EditarCompendioComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'listarDetalles/:idTabla',
    component: ListaDetalleComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'nuevoDetalle/:idTabla',
    component: NuevoDetalleComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'editarDetalle/:id',
    component: EditarDetalleComponent,
    canActivate: [AuthGuard]
  },
  { path: '**', redirectTo: '/listaCompendio', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
