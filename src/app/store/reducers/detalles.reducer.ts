import { Detalle } from '../../models/detalle.model';
import * as fromDetalles from '../actions';
import { createFeatureSelector, createSelector } from '@ngrx/store';
export interface DetallesState {
  data: Detalle[];
  selected: Detalle;
  loaded: boolean;
  loading: boolean;
  error: any;
  action: string;
}
const estadoInicial: DetallesState = {
  data: [],
  selected: null,
  loaded: false,
  loading: false,
  error: null,
  action: null
};

export function detallesReducer(
  state = estadoInicial,
  action: fromDetalles.detallesAcciones
): DetallesState {
  switch (action.type) {
    case fromDetalles.DetalleActionTypes.LOAD_DETALLES:
      return {
        ...state,
        data: [],
        loading: true,
        action: fromDetalles.DetalleActionTypes.LOAD_DETALLES,
        error: null
      };

    case fromDetalles.DetalleActionTypes.LOAD_DETALLES_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.payload
      };
    case fromDetalles.DetalleActionTypes.LOAD_DETALLES_FAIL:
      return {
        ...state,
        loaded: false,
        loading: false,
        error: action.payload
      };
    case fromDetalles.DetalleActionTypes.LOAD_DETALLE:
      return {
        ...state,
        loading: true,
        loaded: false,
        action: fromDetalles.DetalleActionTypes.LOAD_DETALLE,
        error: null
      };
    case fromDetalles.DetalleActionTypes.LOAD_DETALLE_SUCCESS:
      return {
        ...state,
        selected: action.payload,
        loading: false,
        loaded: true
      };
    case fromDetalles.DetalleActionTypes.LOAD_DETALLE_FAIL:
      return {
        ...state,
        loaded: false,
        loading: false,
        error: action.payload
      };

    case fromDetalles.DetalleActionTypes.CREATE_DETALLE:
      return {
        ...state,
        data: [],
        selected: action.payload,
        loading: true,
        loaded: false,
        error: null,
        action: fromDetalles.DetalleActionTypes.CREATE_DETALLE
      };
    case fromDetalles.DetalleActionTypes.CREATE_DETALLE_SUCCESS: {
      return {
        ...state,
        data: [...state.data, action.payload],
        selected: action.payload,
        error: null,
        loading: false,
        loaded: true
      };
    }
    case fromDetalles.DetalleActionTypes.CREATE_DETALLE_FAIL:
      return {
        ...state,
        selected: null,
        loaded: true,
        loading: false,
        error: action.payload
      };

    case fromDetalles.DetalleActionTypes.UPDATE_DETALLE:
      return {
        ...state,
        selected: action.payload,
        loading: true,
        loaded: false,
        error: null,
        action: fromDetalles.DetalleActionTypes.UPDATE_DETALLE
      };
    case fromDetalles.DetalleActionTypes.UPDATE_DETALLE_SUCCESS: {
      const index = state.data.findIndex(
        h => h.ideDetalle === state.selected.ideDetalle
      );

      if (index >= 0) {
        const data = [
          ...state.data.slice(0, index),
          state.selected,
          ...state.data.slice(index + 1)
        ];
        return {
          ...state,
          data,
          selected: action.payload,
          loaded: true,
          loading: false,
          error: null
        };
      }
      return state;
    }
    case fromDetalles.DetalleActionTypes.UPDATE_DETALLE_FAIL:
      return {
        ...state,
        loaded: true,
        loading: false,
        error: action.payload
      };
    case fromDetalles.DetalleActionTypes.DELETE_DETALLE: {
      const selected = state.data.find(h => h.ideDetalle === action.payload);
      return {
        ...state,
        selected,
        action: fromDetalles.DetalleActionTypes.DELETE_DETALLE,
        loaded: false,
        error: null
      };
    }
    case fromDetalles.DetalleActionTypes.DELETE_DETALLE_SUCCESS: {
      const data = state.data.filter(
        h => h.ideDetalle !== state.selected.ideDetalle
      );
      return {
        ...state,
        data,
        selected: null,
        error: null,
        loaded: true
      };
    }
    case fromDetalles.DetalleActionTypes.DELETE_DETALLE_FAIL:
      return {
        ...state,
        selected: null,
        loaded: true,
        error: action.payload
      };

    default:
      return state;
  }
}
/*************************
 * SELECTORS
 ************************/
export const getDetallesState = createFeatureSelector<DetallesState>(
  'detalles'
);

export const getCreateDetalleError = createSelector(
  getDetallesState,
  (state: DetallesState) => {
    return state.action === fromDetalles.DetalleActionTypes.CREATE_DETALLE
      ? state.error
      : null;
  }
);
export const isLoadedCreateDetalle = createSelector(
  getDetallesState,
  (state: DetallesState) =>
    state.action === fromDetalles.DetalleActionTypes.CREATE_DETALLE &&
    state.loaded &&
    !state.error
);
export const isLoadingCreateDetalle = createSelector(
  getDetallesState,
  (state: DetallesState) =>
    state.action === fromDetalles.DetalleActionTypes.CREATE_DETALLE &&
    state.loading &&
    !state.error
);
export const getDetalle = createSelector(
  getDetallesState,
  (state: DetallesState) => {
    if (
      state.action === fromDetalles.DetalleActionTypes.LOAD_DETALLE &&
      state.loaded
    ) {
      return state.selected;
    } else {
      return null;
    }
  }
);
export const isLoadedUpdateDetalle = createSelector(
  getDetallesState,
  (state: DetallesState) =>
    state.action === fromDetalles.DetalleActionTypes.UPDATE_DETALLE &&
    state.loaded &&
    !state.error
);
export const getUpdateDetalleError = createSelector(
  getDetallesState,
  (state: DetallesState) => {
    return state.action === fromDetalles.DetalleActionTypes.UPDATE_DETALLE
      ? state.error
      : null;
  }
);
export const isDeleteDetalle = createSelector(
  getDetallesState,
  (state: DetallesState) =>
    state.action === fromDetalles.DetalleActionTypes.DELETE_DETALLE &&
    state.loaded &&
    !state.error
);

export const isLoadingUpdateDetalle = createSelector(
  getDetallesState,
  (state: DetallesState) =>
    state.action === fromDetalles.DetalleActionTypes.UPDATE_DETALLE &&
    state.loading &&
    !state.error
);
