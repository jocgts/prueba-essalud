import { Compendio } from '../../models/compendio.model';
import * as fromCompendios from '../actions';
import { createFeatureSelector, createSelector } from '@ngrx/store';
export interface CompendiosState {
  data: Compendio[];
  selected: Compendio;
  paginador: any;
  patron: string;
  page: number;
  loaded: boolean;
  loading: boolean;
  error: any;
  action: string;
}

const estadoInicial: CompendiosState = {
  data: [],
  selected: null,
  patron: '',
  page: null,
  paginador: null,
  loaded: false,
  loading: false,
  error: null,
  action: null
};

export function compendiosReducer(
  state = estadoInicial,
  action: fromCompendios.compendiosAcciones
): CompendiosState {
  switch (action.type) {
    case fromCompendios.CompendioActionTypes.LOAD_COMPENDIOS:
      return {
        ...state,
        data: [],
        loading: true,
        patron: '',
        action: fromCompendios.CompendioActionTypes.LOAD_COMPENDIOS,
        page: null,
        error: null
      };

    case fromCompendios.CompendioActionTypes.LOAD_COMPENDIOS_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.payload.compendios,
        paginador: action.payload.paginador
      };

    case fromCompendios.CompendioActionTypes.LOAD_COMPENDIOS_FAIL:
      return {
        ...state,
        loaded: false,
        loading: false,
        error: action.payload
      };
    case fromCompendios.CompendioActionTypes.LOAD_COMPENDIOS_FILTER:
      return {
        ...state,
        data: [],
        loading: true,
        action: fromCompendios.CompendioActionTypes.LOAD_COMPENDIOS_FILTER,
        error: null
      };

    case fromCompendios.CompendioActionTypes.LOAD_COMPENDIOS_FILTER_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        patron: action.payload.patron,
        data: action.payload.compendios,
        paginador: action.payload.paginador
      };

    case fromCompendios.CompendioActionTypes.LOAD_COMPENDIOS_FILTER_FAIL:
      return {
        ...state,
        loaded: false,
        loading: false,
        error: action.payload
      };
    case fromCompendios.CompendioActionTypes.LOAD_COMPENDIO:
      return {
        ...state,
        loading: true,
        loaded: false,
        paginador: null,
        data: [],
        action: fromCompendios.CompendioActionTypes.LOAD_COMPENDIO,
        error: null
      };
    case fromCompendios.CompendioActionTypes.LOAD_COMPENDIO_SUCCESS:
      const data = [...state.data, action.payload];
      return {
        ...state,
        data,
        selected: action.payload,
        loading: false,
        loaded: true
      };
    case fromCompendios.CompendioActionTypes.LOAD_COMPENDIO_FAIL:
      return {
        ...state,
        loaded: false,
        loading: false,
        error: action.payload
      };

    case fromCompendios.CompendioActionTypes.CREATE_COMPENDIO:
      return {
        ...state,
        data: [],
        selected: action.payload,
        loading: true,
        loaded: false,
        error: null,
        action: fromCompendios.CompendioActionTypes.CREATE_COMPENDIO
      };
    case fromCompendios.CompendioActionTypes.CREATE_COMPENDIO_SUCCESS: {
      return {
        ...state,
        data: [...state.data, action.payload],
        selected: action.payload,
        error: null,
        loaded: true
      };
    }
    case fromCompendios.CompendioActionTypes.CREATE_COMPENDIO_FAIL:
      return {
        ...state,
        selected: null,
        loaded: true,
        loading: false,
        error: action.payload
      };

    case fromCompendios.CompendioActionTypes.UPDATE_COMPENDIO:
      return {
        ...state,
        selected: action.payload,
        loading: true,
        loaded: false,
        action: fromCompendios.CompendioActionTypes.UPDATE_COMPENDIO,
        error: null
      };
    case fromCompendios.CompendioActionTypes.UPDATE_COMPENDIO_SUCCESS: {
      const index = state.data.findIndex(h => {
        return h.ideNumericoTabla === state.selected.ideNumericoTabla;
      });

      if (index >= 0) {
        const data = [
          ...state.data.slice(0, index),
          state.selected,
          ...state.data.slice(index + 1)
        ];
        return {
          ...state,
          data,
          loaded: true,
          selected: null,
          error: null
        };
      }
      return state;
    }
    case fromCompendios.CompendioActionTypes.UPDATE_COMPENDIO_FAIL:
      return {
        ...state,
        loaded: true,
        loading: false,
        error: action.payload
      };
    default:
      return state;
  }
}
/*************************
 * SELECTORS
 ************************/
export const getCompendiosState = createFeatureSelector<CompendiosState>(
  'compendios'
);
export const getCreateCompendioError = createSelector(
  getCompendiosState,
  (state: CompendiosState) => {
    return state.action === fromCompendios.CompendioActionTypes.CREATE_COMPENDIO
      ? state.error
      : null;
  }
);
export const isLoadedCreateCompendio = createSelector(
  getCompendiosState,
  (state: CompendiosState) =>
    state.action === fromCompendios.CompendioActionTypes.CREATE_COMPENDIO &&
    state.loaded &&
    !state.error
);
export const isLoadingCreateCompendio = createSelector(
  getCompendiosState,
  (state: CompendiosState) =>
    state.action === fromCompendios.CompendioActionTypes.CREATE_COMPENDIO &&
    state.loading &&
    !state.error
);
export const getCompendio = createSelector(
  getCompendiosState,
  (state: CompendiosState) => {
    if (
      state.action === fromCompendios.CompendioActionTypes.LOAD_COMPENDIO &&
      state.loaded
    ) {
      return state.selected;
    } else {
      return null;
    }
  }
);
export const isLoadedUpdateCompendio = createSelector(
  getCompendiosState,
  (state: CompendiosState) =>
    state.action === fromCompendios.CompendioActionTypes.UPDATE_COMPENDIO &&
    state.loaded &&
    !state.error
);
export const isLoadingUpdateCompendio = createSelector(
  getCompendiosState,
  (state: CompendiosState) =>
    state.action === fromCompendios.CompendioActionTypes.UPDATE_COMPENDIO &&
    state.loading &&
    !state.error
);
export const getUpdateCompendioError = createSelector(
  getCompendiosState,
  (state: CompendiosState) => {
    return state.action === fromCompendios.CompendioActionTypes.UPDATE_COMPENDIO
      ? state.error
      : null;
  }
);
/*export const getCompendiosState = createFeatureSelector<CompendiosState>(
  'compendios'
);
export const getAllCopendios = createSelector(
  getCompendiosState,
  (state: State) => state.data
);
export const getCompendio = createSelector(
  getCompendiosState,
  (state: State) => {
    if (
      state.action === fromCompendios.CompendioActionTypes.LOAD_COMPENDIO &&
      state.loaded
    ) {
      return state.selected;
    } else {
      return null;
    }
  }
);

export const isCreated = createSelector(
  getCompendiosState,
  (state: State) =>
    state.action === fromCompendios.CompendioActionTypes.CREATE_COMPENDIO &&
    state.loaded &&
    !state.error
);
export const isUpdated = createSelector(
  getCompendiosState,
  (state: State) =>
    state.action === fromCompendios.CompendioActionTypes.UPDATE_COMPENDIO &&
    state.loaded &&
    !state.error
);

export const getCreateError = createSelector(
  getCompendiosState,
  (state: CompendiosState) => {
    return state.action === fromCompendios.CompendioActionTypes.CREATE_COMPENDIO
      ? state.error
      : null;
  }
);

export const getUpdateError = createSelector(
  getCompendiosState,
  (state: State) => {
    return state.action === fromCompendios.CompendioActionTypes.UPDATE_COMPENDIO
      ? state.error
      : null;
  }
);
export const getCompendiosError = createSelector(
  getCompendiosState,
  (state: State) => {
    return state.action === fromCompendios.CompendioActionTypes.LOAD_COMPENDIOS
      ? state.error
      : null;
  }
);
export const getCompendioError = createSelector(
  getCompendiosState,
  (state: State) => {
    return state.action === fromCompendios.CompendioActionTypes.LOAD_COMPENDIO
      ? state.error
      : null;
  }
);
*/
