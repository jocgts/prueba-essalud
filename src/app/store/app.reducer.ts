import { ActionReducerMap } from '@ngrx/store';
import * as reducers from './reducers';

export interface AppState {
  compendios: reducers.CompendiosState;
  detalles: reducers.DetallesState;
}

export const appReducers: ActionReducerMap<AppState> = {
  compendios: reducers.compendiosReducer,
  detalles: reducers.detallesReducer
};
