import { Action } from '@ngrx/store';
import { Detalle } from '../../models/detalle.model';

export enum DetalleActionTypes {
  LOAD_DETALLES = '[DetalleCompendio] Load Detalles',
  LOAD_DETALLES_SUCCESS = '[DetalleCompendio] Load Detalles Success',
  LOAD_DETALLES_FAIL = '[DetalleCompendio] Load Detalles Fail',
  LOAD_DETALLE = '[DetalleCompendio] Load Detalle',
  LOAD_DETALLE_SUCCESS = '[DetalleCompendio] Load Detalle Success',
  LOAD_DETALLE_FAIL = '[DetalleCompendio] Load Detalle Fail',
  CREATE_DETALLE = '[DetalleCompendio] Create Detalle',
  CREATE_DETALLE_SUCCESS = '[DetalleCompendio] Create Detalle Success',
  CREATE_DETALLE_FAIL = '[DetalleCompendio] Create Detalle Fail',
  UPDATE_DETALLE = '[DetalleCompendio] Update Detalle',
  UPDATE_DETALLE_SUCCESS = '[DetalleCompendio] Update Detalle Success',
  UPDATE_DETALLE_FAIL = '[DetalleCompendio] Update Detalle Fail',
  DELETE_DETALLE = '[DetalleCompendio] Delete Detalle',
  DELETE_DETALLE_SUCCESS = '[DetalleCompendio] Delete Detalle Success',
  DELETE_DETALLE_FAIL = '[DetalleCompendio] Delete Detalle Fail'
}

export class LoadDetalles implements Action {
  readonly type = DetalleActionTypes.LOAD_DETALLES;
  constructor(public payload: number) {}
}
export class LoadDetallesSuccess implements Action {
  readonly type = DetalleActionTypes.LOAD_DETALLES_SUCCESS;
  constructor(public payload: Detalle[]) {}
}
export class LoadDetallesFail implements Action {
  readonly type = DetalleActionTypes.LOAD_DETALLES_FAIL;
  constructor(public payload: any) {}
}
//
export class LoadDetalle implements Action {
  readonly type = DetalleActionTypes.LOAD_DETALLE;
  constructor(public payload: number) {}
}

export class LoadDetalleSuccess implements Action {
  readonly type = DetalleActionTypes.LOAD_DETALLE_SUCCESS;
  constructor(public payload: Detalle) {}
}

export class LoadDetalleFail implements Action {
  readonly type = DetalleActionTypes.LOAD_DETALLE_FAIL;
  constructor(public payload: any) {}
}
//
export class CreateDetalle implements Action {
  readonly type = DetalleActionTypes.CREATE_DETALLE;
  constructor(public payload: Detalle) {}
}
export class CreateDetalleSuccess implements Action {
  readonly type = DetalleActionTypes.CREATE_DETALLE_SUCCESS;
  constructor(public payload: Detalle) {}
}

export class CreateDetalleFail implements Action {
  readonly type = DetalleActionTypes.CREATE_DETALLE_FAIL;
  constructor(public payload: any) {}
}
//
export class UpdateDetalle implements Action {
  readonly type = DetalleActionTypes.UPDATE_DETALLE;
  constructor(public payload: Detalle) {}
}

export class UpdateDetalleFail implements Action {
  readonly type = DetalleActionTypes.UPDATE_DETALLE_FAIL;
  constructor(public payload: any) {}
}

export class UpdateDetalleSuccess implements Action {
  readonly type = DetalleActionTypes.UPDATE_DETALLE_SUCCESS;
  constructor(public payload: Detalle) {}
}

//
export class DeleteDetalle implements Action {
  readonly type = DetalleActionTypes.DELETE_DETALLE;
  constructor(public payload: number) {}
}
export class DeleteDetalleSuccess implements Action {
  readonly type = DetalleActionTypes.DELETE_DETALLE_SUCCESS;
}
export class DeleteDetalleFail implements Action {
  readonly type = DetalleActionTypes.DELETE_DETALLE_FAIL;
  constructor(public payload: any) {}
}

export type detallesAcciones =
  | LoadDetalles
  | LoadDetallesSuccess
  | LoadDetallesFail
  | LoadDetalle
  | LoadDetalleSuccess
  | LoadDetalleFail
  | CreateDetalle
  | CreateDetalleSuccess
  | CreateDetalleFail
  | UpdateDetalle
  | UpdateDetalleSuccess
  | UpdateDetalleFail
  | DeleteDetalle
  | DeleteDetalleSuccess
  | DeleteDetalleFail;
