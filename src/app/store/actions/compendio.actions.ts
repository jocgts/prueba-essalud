import { Action } from '@ngrx/store';
import { Compendio } from '../../models/compendio.model';

export enum CompendioActionTypes {
  LOAD_COMPENDIOS = '[Compendio] Load Compendios',
  LOAD_COMPENDIOS_SUCCESS = '[Compendio] Load Compendios Success',
  LOAD_COMPENDIOS_FAIL = '[Compendio] Load Compendios Fail',
  LOAD_COMPENDIOS_FILTER = '[Compendio] Load Compendios Filter',
  LOAD_COMPENDIOS_FILTER_SUCCESS = '[Compendio] Load Compendios Filter Success',
  LOAD_COMPENDIOS_FILTER_FAIL = '[Compendio] Load Compendios Filter Fail',
  LOAD_COMPENDIO = '[Compendio] Load Compendio',
  LOAD_COMPENDIO_SUCCESS = '[Compendio] Load Compendio Success',
  LOAD_COMPENDIO_FAIL = '[Compendio] Load Compendio Fail',
  CREATE_COMPENDIO = '[Compendio] Create Compendio',
  CREATE_COMPENDIO_SUCCESS = '[Compendio] Create Compendio Success',
  CREATE_COMPENDIO_FAIL = '[Compendio] Create Compendio Fail',
  UPDATE_COMPENDIO = '[Compendio] Update Compendio',
  UPDATE_COMPENDIO_SUCCESS = '[Compendio] Update Compendio Success',
  UPDATE_COMPENDIO_FAIL = '[Compendio] Update Compendio Fail'
}

export class LoadCompendios implements Action {
  readonly type = CompendioActionTypes.LOAD_COMPENDIOS;
  constructor(public payload: number) {}
}
export class LoadCompendiosSuccess implements Action {
  readonly type = CompendioActionTypes.LOAD_COMPENDIOS_SUCCESS;
  constructor(public payload: { compendios: Compendio[]; paginador: any }) {}
}
export class LoadCompendiosFail implements Action {
  readonly type = CompendioActionTypes.LOAD_COMPENDIOS_FAIL;
  constructor(public payload: any) {}
}

//
export class LoadCompendiosFilter implements Action {
  readonly type = CompendioActionTypes.LOAD_COMPENDIOS_FILTER;
  constructor(public payload: { term: string; page: number }) {}
}
export class LoadCompendiosFilterSuccess implements Action {
  readonly type = CompendioActionTypes.LOAD_COMPENDIOS_FILTER_SUCCESS;
  constructor(
    public payload: { compendios: Compendio[]; paginador: any; patron: string }
  ) {}
}
export class LoadCompendiosFilterFail implements Action {
  readonly type = CompendioActionTypes.LOAD_COMPENDIOS_FILTER_FAIL;
  constructor(public payload: any) {}
}
//
export class CreateCompendio implements Action {
  readonly type = CompendioActionTypes.CREATE_COMPENDIO;
  constructor(public payload: Compendio) {}
}
export class CreateCompendioSuccess implements Action {
  readonly type = CompendioActionTypes.CREATE_COMPENDIO_SUCCESS;
  constructor(public payload: Compendio) {}
}

export class CreateCompendioFail implements Action {
  readonly type = CompendioActionTypes.CREATE_COMPENDIO_FAIL;
  constructor(public payload: any) {}
}
//
export class UpdateCompendio implements Action {
  readonly type = CompendioActionTypes.UPDATE_COMPENDIO;
  constructor(public payload: Compendio) {}
}

export class UpdateCompendioFail implements Action {
  readonly type = CompendioActionTypes.UPDATE_COMPENDIO_FAIL;
  constructor(public payload: any) {}
}

export class UpdateCompendioSuccess implements Action {
  readonly type = CompendioActionTypes.UPDATE_COMPENDIO_SUCCESS;
  constructor(public payload: Compendio) {}
}

//
export class LoadCompendio implements Action {
  readonly type = CompendioActionTypes.LOAD_COMPENDIO;
  constructor(public payload: number) {}
}

export class LoadCompendioSuccess implements Action {
  readonly type = CompendioActionTypes.LOAD_COMPENDIO_SUCCESS;
  constructor(public payload: Compendio) {}
}

export class LoadCompendioFail implements Action {
  readonly type = CompendioActionTypes.LOAD_COMPENDIO_FAIL;
  constructor(public payload: string[]) {}
}

export type compendiosAcciones =
  | LoadCompendios
  | LoadCompendiosSuccess
  | LoadCompendiosFail
  | LoadCompendiosFilter
  | LoadCompendiosFilterSuccess
  | LoadCompendiosFilterFail
  | LoadCompendio
  | LoadCompendioSuccess
  | LoadCompendioFail
  | CreateCompendio
  | CreateCompendioSuccess
  | CreateCompendioFail
  | UpdateCompendio
  | UpdateCompendioSuccess
  | UpdateCompendioFail;
