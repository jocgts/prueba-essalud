import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import * as compendioActions from '../actions';
import { Observable, of } from 'rxjs';
import { Action } from '@ngrx/store';
import { CodigoService } from '../../services/codigo.service';
import { catchError, map, switchMap } from 'rxjs/operators';
import { Compendio } from '../../models/compendio.model';
@Injectable()
export class CompendiosEffects {
  constructor(private actions$: Actions, private cvc: CodigoService) {}

  @Effect()
  loadCompendios$: Observable<Action> = this.actions$.pipe(
    ofType(compendioActions.CompendioActionTypes.LOAD_COMPENDIOS),
    switchMap(action => {
      const page = action['payload'];
      return this.cvc.getCompendios(page).pipe(
        map((response: any) => {
          const payload = {
            compendios: response.content,
            paginador: response
          };
          return new compendioActions.LoadCompendiosSuccess(payload);
        }),

        catchError(err => [new compendioActions.LoadCompendiosFail(err)])
      );
    })
  );

  @Effect()
  loadCompendiosFilter$: Observable<Action> = this.actions$.pipe(
    ofType(compendioActions.CompendioActionTypes.LOAD_COMPENDIOS_FILTER),
    switchMap(action => {
      console.log(action);
      const data = action['payload'];

      return this.cvc.filtrarCompendios(data['term'], data['page']).pipe(
        map((response: any) => {
          const payload = {
            compendios: response.content,
            paginador: response,
            patron: data['term']
          };
          return new compendioActions.LoadCompendiosFilterSuccess(payload);
        }),

        catchError(err => [new compendioActions.LoadCompendiosFilterFail(err)])
      );
    })
  );

  @Effect()
  loadCompendio$ = this.actions$
    .pipe(ofType(compendioActions.CompendioActionTypes.LOAD_COMPENDIO))
    .pipe(
      switchMap(action => {
        const id = action['payload'];
        return this.cvc.getCompendio(id).pipe(
          map(
            compendio => new compendioActions.LoadCompendioSuccess(compendio)
          ),
          catchError(err => of(new compendioActions.LoadCompendioFail(err)))
        );
      })
    );
  @Effect()
  createCompendio$ = this.actions$
    .pipe(ofType(compendioActions.CompendioActionTypes.CREATE_COMPENDIO))
    .pipe(
      switchMap(action => {
        const compendioPara: Compendio = action['payload'];
        return this.cvc.saveCompendio(compendioPara).pipe(
          map(
            compendio => new compendioActions.CreateCompendioSuccess(compendio)
          ),
          catchError(err => [new compendioActions.CreateCompendioFail(err)])
        );
      })
    );

  @Effect()
  updateCompendio$ = this.actions$
    .pipe(ofType(compendioActions.CompendioActionTypes.UPDATE_COMPENDIO))
    .pipe(
      switchMap(action => {
        const compendioPara: Compendio = action['payload'];
        return this.cvc.saveCompendio(compendioPara).pipe(
          map(
            compendio => new compendioActions.UpdateCompendioSuccess(compendio)
          ),
          catchError(err => [new compendioActions.UpdateCompendioFail(err)])
        );
      })
    );
}
