import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import * as detalleActions from '../actions';
import { Observable, of } from 'rxjs';
import { Action } from '@ngrx/store';
import { CodigoService } from '../../services/codigo.service';
import { catchError, map, switchMap } from 'rxjs/operators';
import { Detalle } from '../../models/detalle.model';
@Injectable()
export class DetallesEffects {
  constructor(private actions$: Actions, private cvc: CodigoService) {}

  @Effect()
  loadDetalles$ = this.actions$
    .pipe(ofType(detalleActions.DetalleActionTypes.LOAD_DETALLES))
    .pipe(
      switchMap(action => {
        const ide = action['payload'];
        return this.cvc.getDetallesPorCompendio(ide).pipe(
          map(detalles => new detalleActions.LoadDetallesSuccess(detalles)),
          catchError(err => [new detalleActions.LoadDetallesFail(err)])
        );
      })
    );

  @Effect()
  loadDetalle$ = this.actions$
    .pipe(ofType(detalleActions.DetalleActionTypes.LOAD_DETALLE))
    .pipe(
      switchMap(action => {
        const id = action['payload'];
        return this.cvc.getDetalle(id).pipe(
          map(detalle => new detalleActions.LoadDetalleSuccess(detalle)),
          catchError(err => of(new detalleActions.LoadDetalleFail(err)))
        );
      })
    );
  @Effect()
  createDetalle$ = this.actions$
    .pipe(ofType(detalleActions.DetalleActionTypes.CREATE_DETALLE))
    .pipe(
      switchMap(action => {
        const detallePara: Detalle = action['payload'];
        return this.cvc.saveDetalle(detallePara).pipe(
          map(detalle => new detalleActions.CreateDetalleSuccess(detalle)),
          catchError(err => [new detalleActions.CreateDetalleFail(err)])
        );
      })
    );

  @Effect()
  updateDetalle$ = this.actions$
    .pipe(ofType(detalleActions.DetalleActionTypes.UPDATE_DETALLE))
    .pipe(
      switchMap(action => {
        const detallePara: Detalle = action['payload'];
        return this.cvc.saveDetalle(detallePara).pipe(
          map(detalle => new detalleActions.UpdateDetalleSuccess(detalle)),
          catchError(err => [new detalleActions.UpdateCompendioFail(err.error)])
        );
      })
    );

  @Effect()
  deleteDetalle$ = this.actions$
    .pipe(ofType(detalleActions.DetalleActionTypes.DELETE_DETALLE))
    .pipe(
      switchMap(action => {
        const id = action['payload'];
        return this.cvc.deleteDetalle(id).pipe(
          map(() => new detalleActions.DeleteDetalleSuccess()),
          catchError(err => [new detalleActions.DeleteDetalleFail(err)])
        );
      })
    );
}
