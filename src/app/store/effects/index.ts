import { CompendiosEffects } from './compendios.effects';
import { DetallesEffects } from './detalles.effects';
export const effectsArr: any[] = [CompendiosEffects, DetallesEffects];

export * from './compendios.effects';
export * from './detalles.effects';
