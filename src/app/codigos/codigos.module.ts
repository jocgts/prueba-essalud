import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListaCompendioComponent } from './compendio/lista-compendio/lista-compendio.component';
import { NuevoCompendioComponent } from './compendio/nuevo-compendio/nuevo-compendio.component';
import { EditarCompendioComponent } from './compendio/editar-compendio/editar-compendio.component';
import { ListaDetalleComponent } from './detalle/lista-detalle/lista-detalle.component';
import { NuevoDetalleComponent } from './detalle/nuevo-detalle/nuevo-detalle.component';
import { EditarDetalleComponent } from './detalle/editar-detalle/editar-detalle.component';
import { PaginatorComponent } from './compendio/paginator/paginator.component';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { dateFormatPipe } from './pipes/dateFormatPipe';
import { MatDatepickerModule } from '@angular/material';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
@NgModule({
  declarations: [
    ListaCompendioComponent,
    NuevoCompendioComponent,
    EditarCompendioComponent,
    ListaDetalleComponent,
    NuevoDetalleComponent,
    EditarDetalleComponent,
    PaginatorComponent,
    dateFormatPipe
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    FontAwesomeModule,
    MatDatepickerModule,
    MatMomentDateModule
  ],
  exports: [
    ListaCompendioComponent,
    NuevoCompendioComponent,
    EditarCompendioComponent,
    ListaDetalleComponent,
    NuevoDetalleComponent,
    EditarDetalleComponent,
    RouterModule,
    PaginatorComponent
  ]
})
export class CodigosModule {}
