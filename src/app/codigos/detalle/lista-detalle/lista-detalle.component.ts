import { Component, OnInit } from '@angular/core';
import { Detalle } from 'src/app/models/detalle.model';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { AppState } from '../../../store/app.reducer';
import * as actions from '../../../store/actions';
import { faEraser, faEdit } from '@fortawesome/free-solid-svg-icons';
import { getCompendio } from '../../../store/reducers/compendios.reducer';
import { isDeleteDetalle } from '../../../store/reducers/detalles.reducer';
import swal from 'sweetalert2';
import { Compendio } from 'src/app/models/compendio.model';
@Component({
  selector: 'app-lista-detalle',
  templateUrl: './lista-detalle.component.html',
  styles: []
})
export class ListaDetalleComponent implements OnInit {
  detalles: Detalle[];
  compendioSeleccionado: Compendio;
  ideTabla: number;
  loading: boolean;
  error: any;
  faEraser = faEraser;
  faEdit = faEdit;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private store: Store<AppState>
  ) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.ideTabla = params.idTabla;
      this.store.dispatch(new actions.LoadDetalles(params.idTabla));
      this.store.dispatch(new actions.LoadCompendio(params.idTabla));
    });
    this.store
      .select(getCompendio)
      .subscribe(compendio => (this.compendioSeleccionado = compendio));
    this.store.select('detalles').subscribe(comp => {
      this.detalles = comp.data;
      this.loading = comp.loading;
      this.error = comp.error;
    });
    this.store.select(isDeleteDetalle).subscribe(isErased => {
      if (isErased) {
        swal.fire(
          'Eliminar Detalle',
          'Se Elimino Detalle del Compendio',
          'success'
        );
      }
    });
  }
  eliminarDetalle(ideDetalle: number) {
    swal
      .fire({
        title: 'Estas Seguro?',
        text: '¿Seguro que desea eliminar el Detalle?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, Eliminar!'
      })
      .then(result => {
        if (result.value) {
          this.store.dispatch(new actions.DeleteDetalle(ideDetalle));
        }
      });
  }
  volver() {
    this.router.navigate(['/listaCompendio']);
  }
}
