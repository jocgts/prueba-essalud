import { Component, OnInit } from '@angular/core';
import { Detalle } from 'src/app/models/detalle.model';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { AppState } from '../../../store/app.reducer';
import * as detallesActions from '../../../store/actions';
import swal from 'sweetalert2';
import {
  getDetalle,
  getUpdateDetalleError,
  isLoadedUpdateDetalle,
  isLoadingUpdateDetalle
} from '../../../store/reducers/detalles.reducer';
// @ts-ignore
import { default as _rollupMoment } from 'moment';
import * as _moment from 'moment';

const moment = _rollupMoment || _moment;

@Component({
  selector: 'app-editar-detalle',
  templateUrl: './editar-detalle.component.html',
  styles: []
})
export class EditarDetalleComponent implements OnInit {
  title = 'Actualizar Detalle';
  detalle: Detalle;
  loading: boolean;
  keys: any;
  error: any;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private store: Store<AppState>
  ) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.store.dispatch(new detallesActions.LoadDetalle(params.id));
    });
    this.store.select(getDetalle).subscribe(detalle => {
      if (detalle) {
        this.detalle = detalle;
      }
    });
    this.store.select(getUpdateDetalleError).subscribe(error => {
      if (error) {
        if (error.status === '400') {
          this.keys = Object.keys(error.error);
          window.scroll(0, 0);
        } else {
          swal.fire(
            'Lo Sentimos',
            'Se presento un error, Comunicarse con el administrador',
            'error'
          );
        }
      }
      this.error = error;
    });
    this.store.select(isLoadedUpdateDetalle).subscribe(isCreated => {
      if (isCreated) {
        swal.fire(
          'Edicion Detalle',
          'Se Actualizo Detalle del Compendio',
          'success'
        );
        this.router.navigate([
          '/listarDetalles',
          this.detalle.ideNumericoTabla
        ]);
      }
    });
    this.store
      .select(isLoadingUpdateDetalle)
      .subscribe(isLoading => (this.loading = isLoading));
  }
  onUpdateDetalle() {
    const fecInicvigeCodigo = moment(
      new Date(this.detalle.fecInicvigeCodigo)
    ).format('YYYYMMDD');
    if (fecInicvigeCodigo !== 'Invalid date') {
      this.detalle.fecInicvigeCodigo = fecInicvigeCodigo;
    }

    this.store.dispatch(new detallesActions.UpdateDetalle(this.detalle));
  }
  volver() {
    this.router.navigate(['/listarDetalles', this.detalle.ideNumericoTabla]);
  }
}
