import { Component, OnInit } from '@angular/core';
import { Detalle } from 'src/app/models/detalle.model';
import swal from 'sweetalert2';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { AppState } from '../../../store/app.reducer';
import {
  getCreateDetalleError,
  isLoadedCreateDetalle,
  isLoadingCreateDetalle
} from '../../../store/reducers/detalles.reducer';
import * as detallesActions from '../../../store/actions';
// @ts-ignore
import { default as _rollupMoment } from 'moment';
import * as _moment from 'moment';

const moment = _rollupMoment || _moment;

@Component({
  selector: 'app-nuevo-detalle',
  templateUrl: './nuevo-detalle.component.html',
  styles: []
})
export class NuevoDetalleComponent implements OnInit {
  detalle: Detalle;
  title = 'Crear Nuevo Detalle';
  error: any;
  keys: any;
  loading: boolean;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private store: Store<AppState>
  ) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.detalle = new Detalle();
      this.detalle.ideNumericoTabla = params.idTabla;
    });
    this.store.select(getCreateDetalleError).subscribe(error => {
      if (error) {
        this.keys = Object.keys(error.error);
        window.scroll(0, 0);
      }
      this.error = error;
    });
    this.store.select(isLoadedCreateDetalle).subscribe(isCreated => {
      if (isCreated) {
        swal.fire(
          'Nuevo Detalle',
          'Se registro un nuevo Detalle del Compendio',
          'success'
        );
        this.router.navigate([
          '/listarDetalles',
          this.detalle.ideNumericoTabla
        ]);
      }
    });
    this.store
      .select(isLoadingCreateDetalle)
      .subscribe(isLoading => (this.loading = isLoading));
  }
  onCreateDetalle() {
    /*const fecInicvigeCodigo = new Date(this.detalle.fecInicvigeCodigo);
    this.detalle.fecInicvigeCodigo = moment(fecInicvigeCodigo).format(
      'YYYYMMDD'
    );*/

    const fecInicvigeCodigo = moment(
      new Date(this.detalle.fecInicvigeCodigo)
    ).format('YYYYMMDD');
    if (fecInicvigeCodigo !== 'Invalid date') {
      this.detalle.fecInicvigeCodigo = fecInicvigeCodigo;
    }

    this.store.dispatch(new detallesActions.CreateDetalle(this.detalle));
  }
  volver() {
    this.router.navigate(['/listarDetalles', this.detalle.ideNumericoTabla]);
  }
}
