import { Component, OnInit } from '@angular/core';
import { Compendio } from 'src/app/models/compendio.model';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { AppState } from '../../../store/app.reducer';
import * as compendiosActions from '../../../store/actions';
import {
  getUpdateCompendioError,
  isLoadedUpdateCompendio,
  isLoadingUpdateCompendio,
  getCompendio
} from '../../../store/reducers/compendios.reducer';
import swal from 'sweetalert2';
// @ts-ignore
import { default as _rollupMoment } from 'moment';
import * as _moment from 'moment';

const moment = _rollupMoment || _moment;
@Component({
  selector: 'app-editar-compendio',
  templateUrl: './editar-compendio.component.html',
  styles: []
})
export class EditarCompendioComponent implements OnInit {
  title = 'Actualizar Compendio';
  compendio: Compendio;
  loading: boolean;
  keys: any;
  error: any;
  constructor(
    private route: ActivatedRoute,
    private store: Store<AppState>,
    private router: Router
  ) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.store.dispatch(new compendiosActions.LoadCompendio(params.id));
    });
    this.store.select(getCompendio).subscribe(compendio => {
      this.compendio = compendio;
    });
    this.store.select(getUpdateCompendioError).subscribe(error => {
      if (error) {
        if (error.status === '400') {
          this.keys = Object.keys(error.error);
          window.scroll(0, 0);
        } else {
          swal.fire(
            'Lo Sentimos',
            'Se presento un error, Comunicarse con el administrador',
            'error'
          );
        }
      }
      this.error = error;
    });
    this.store.select(isLoadedUpdateCompendio).subscribe(IsLoaded => {
      if (IsLoaded) {
        swal.fire('Edicion Compendio', 'Se Actualizo el Compendio', 'success');
        this.router.navigate(['/listaCompendio']);
      }
    });
    this.store
      .select(isLoadingUpdateCompendio)
      .subscribe(isLoading => (this.loading = isLoading));
  }
  onSaveCompendio() {
    const fecInicvigeTabla = moment(
      new Date(this.compendio.fecInicvigeTabla)
    ).format('YYYYMMDD');
    if (fecInicvigeTabla !== 'Invalid date') {
      this.compendio.fecInicvigeTabla = fecInicvigeTabla;
    }

    this.store.dispatch(new compendiosActions.UpdateCompendio(this.compendio));
  }
  volver() {
    this.router.navigate(['/listaCompendio']);
  }
}
