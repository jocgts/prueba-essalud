import { Component, OnInit } from '@angular/core';
import { Compendio } from 'src/app/models/compendio.model';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { AppState } from '../../../store/app.reducer';
import {
  getCreateCompendioError,
  isLoadedCreateCompendio,
  isLoadingCreateCompendio
} from '../../../store/reducers/compendios.reducer';
import * as compendiosActions from '../../../store/actions';
import swal from 'sweetalert2';
import { faSync } from '@fortawesome/free-solid-svg-icons';
// @ts-ignore
import { default as _rollupMoment } from 'moment';
import * as _moment from 'moment';

const moment = _rollupMoment || _moment;

@Component({
  selector: 'app-nuevo-compendio',
  templateUrl: './nuevo-compendio.component.html',
  styles: ['./nuevo-compendio.component.css']
})
export class NuevoCompendioComponent implements OnInit {
  title = 'Crear Nuevo Compendio';
  compendio: Compendio = new Compendio();
  error: any;
  keys: any;
  loading: boolean;
  constructor(private router: Router, private store: Store<AppState>) {}

  ngOnInit() {
    this.store.select(getCreateCompendioError).subscribe(error => {
      if (error) {
        if (error.status === '400') {
          this.keys = Object.keys(error.error);
          window.scroll(0, 0);
        } else {
          swal.fire(
            'Lo Sentimos',
            'Se presento un error, Comunicarse con el administrador',
            'error'
          );
        }
      }
    });

    this.store.select(isLoadedCreateCompendio).subscribe(isCreated => {
      if (isCreated) {
        swal
          .fire({
            title: 'Se registro con Exito el Compendio',
            text: 'Desea ingresar un nuevo detalle al compendio',
            type: 'question',
            showCancelButton: true,
            confirmButtonText: 'Si',
            cancelButtonText: 'NO'
          })
          .then(result => {
            if (result.value) {
              this.router.navigate([
                `/nuevoDetalle/${this.compendio.ideNumericoTabla}`
              ]);
            } else if (result.dismiss === swal.DismissReason.cancel) {
              this.router.navigate(['/listaCompendio']);
            }
          });
      }
    });
    this.store
      .select(isLoadingCreateCompendio)
      .subscribe(isLoading => (this.loading = isLoading));
  }
  onCreateCompendio() {
    const fecInicvigeTabla = new Date(this.compendio.fecInicvigeTabla);
    this.compendio.fecInicvigeTabla = moment(fecInicvigeTabla).format(
      'YYYYMMDD'
    );
    this.store.dispatch(new compendiosActions.CreateCompendio(this.compendio));
  }

  volver() {
    this.router.navigate(['/listaCompendio']);
  }
  reset() {
    this.compendio.ideNumericoTabla = null;
    this.compendio.txtDescripcTabla = '';
    this.compendio.txtIdentifi1 = '';
    this.compendio.txtIdentifi2 = '';
    this.compendio.txtIdentifi3 = '';
    this.compendio.txtIdentifi4 = '';
  }
}
