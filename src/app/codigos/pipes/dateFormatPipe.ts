import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dateFormatPipe'
})
export class dateFormatPipe implements PipeTransform {
  transform(value: string) {
    if (value) {
      return (
        value.substring(6) +
        '/' +
        value.substring(4, 6) +
        '/' +
        value.substring(0, 4)
      );
    }

    return '';
  }
}
