import { Component, OnInit, ɵConsole } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Compendio } from 'src/app/models/compendio.model';
import { faInfoCircle, faEdit } from '@fortawesome/free-solid-svg-icons';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/app.reducer';
import * as compendiosActions from '../../store/actions';

@Component({
  selector: 'app-lista-compendio',
  templateUrl: './lista-compendio.component.html',
  styles: ['./lista-compendio.component.css']
})
export class ListaCompendioComponent implements OnInit {
  compendios: Compendio[];
  paginador: any;
  loading: boolean;
  error: any;
  patron: string;
  faInfoCircle = faInfoCircle;
  faEdit = faEdit;
  constructor(
    private activatedRoute: ActivatedRoute,
    private store: Store<AppState>
  ) {}

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(params => {
      let page: number = +params.get('page');

      if (!page) {
        page = 0;
      }
      this.store.select('compendios').subscribe(comp => {
        this.compendios = comp.data;
        this.paginador = comp.paginador;
        this.loading = comp.loading;
        this.patron = comp.patron;
        this.error = comp.error;
      });
      if (!this.patron) {
        this.store.dispatch(new compendiosActions.LoadCompendios(page));
      } else {
        const payload = {
          term: this.patron,
          page
        };
        this.store.dispatch(
          new compendiosActions.LoadCompendiosFilter(payload)
        );
      }
    });
  }
  buscarCompendiosID(id: number) {
    this.store.dispatch(new compendiosActions.LoadCompendio(id));
  }
  buscarCompendiosDescrip(term: string) {
    console.log(term, '***');
    const payload = {
      term,
      page: 0
    };
    this.store.dispatch(new compendiosActions.LoadCompendiosFilter(payload));
  }
}
