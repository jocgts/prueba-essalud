export class Detalle {
  ideDetalle: number;
  ideNumericoTabla: number;
  codElementoTabla: string;
  txtDescripcCodigo: string;
  txtDescripcCorto: string;
  flgElementoGrupo: string;
  txtReferenc1: string;
  txtReferenc2: string;
  txtReferenc3: string;
  txtReferenc4: string;
  fecInicvigeCodigo: string;
  fecTermvigeCodigo: string;
  txtReferenc5: string;
  txtReferenc6: string;
  txtReferenc7: string;
  txtReferenc8: string;
}
