export class Compendio {
  ideNumericoTabla: number;
  txtIdentifi1: string;
  txtIdentifi2: string;
  txtIdentifi3: string;
  txtIdentifi4: string;
  fecInicvigeTabla: string;
  fecTermvigeTabla: string;
  txtDescripcTabla: string;
  txtReferenc1: string;
  txtReferenc2: string;
  flgEstado: string;
}
