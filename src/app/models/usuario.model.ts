export class Usuario {
  idUsuario: string;
  email: string;
  ruc: string;
  token: string;
  roles: string[] = [];
}
