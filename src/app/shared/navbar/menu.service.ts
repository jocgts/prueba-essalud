import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Menu } from './menu';
@Injectable({
  providedIn: 'root'
})
export class MenuService {
  private environmentUrl = environment.apiUrl;
  constructor(private http: HttpClient) {}
  getMenu(): Observable<Menu> {
    return this.http.get<Menu>(this.environmentUrl + '/menu');
  }
}
