import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { MenuService } from './menu.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styles: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  public items: MenuItem[];
  constructor(private menuService: MenuService) {}
  ngOnInit() {
    this.menuService.getMenu().subscribe(menus => {
      this.items = menus.parents;
    });
  }
}
