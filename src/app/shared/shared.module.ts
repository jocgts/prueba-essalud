import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './navbar/navbar.component';
import { TieredMenuModule } from 'primeng/tieredmenu';
import { ButtonModule } from 'primeng/button';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
@NgModule({
  declarations: [NavbarComponent],
  imports: [CommonModule, TieredMenuModule, ButtonModule, HttpClientModule],
  exports: [NavbarComponent]
})
export class SharedModule {}
